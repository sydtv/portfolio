// SHOW MENU
const showMenu = (toggleId, navId) => {
    const toggle = document.getElementById(toggleId);
    const nav = document.getElementById(navId);

    if (toggle && nav) {
        toggle.addEventListener('click', () => {
            nav.classList.toggle('show-menu');
        });
    }
}

showMenu('nav-toggle', 'nav-menu');

// REMOVE MENU
const navLink = document.querySelectorAll('.nav__link');

const linkAction = () => {
    const navMenu = document.getElementById('nav-menu');
    navMenu.classList.remove('show-menu');
}

navLink.forEach(n => n.addEventListener('click', linkAction));

// CHANGE BACKGROUND HEADER
const scrollHeader = () => {
    const header = document.getElementById('header');

    if (this.scrollY >= 200) {
        header.classList.add('scroll-header');
    } else {
        header.classList.remove('scroll-header');
    }
}

window.addEventListener('scroll', scrollHeader);

// SHOW SCROLL TOP
const scrollTop = () => {
    const scrollTop = document.getElementById('scroll-top');
    if (this.scrollY >= 560) {
        scrollTop.classList.add('show-scroll');
    } else {
        scrollTop.classList.remove('show-scroll');
    }
}

window.addEventListener('scroll', scrollTop);

// PORTFOLIO FILTER
const containerEl = document.querySelector('.portfolio__container');
let mixer;

if (containerEl) {
    mixer = mixitup(containerEl, {
        selectors: {
            target: '.portfolio__content'
        },
        animation: {
            duration: 400
        }
    });
}

// GSAP ANIMATIONS
gsap.from('.home__img', {
    opacity: 0,
    duration: 2,
    delay: .5,
    x: 60
});

gsap.from('.home__data', {
    opacity: 0,
    duration: 2,
    delay: .8,
    y: 25
});

gsap.from('.home__greeting, .home__name, .home__description, .home__button', {
    opacity: 0,
    duration: 2,
    delay: 1,
    y: 25,
    ease: 'expo.out',
    stagger: .2
});

gsap.from('.nav__logo, .nav__toggle', {
    opacity: 0,
    duration: 2,
    delay: 1.5,
    y: 25,
    ease: 'expo.out',
    stagger: .2
});

gsap.from('.nav__item', {
    opacity: 0,
    duration: 2,
    delay: 1.8,
    y: 25,
    ease: 'expo.out',
    stagger: .2
});